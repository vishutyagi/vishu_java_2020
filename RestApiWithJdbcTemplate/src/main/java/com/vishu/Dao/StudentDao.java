package com.vishu.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.vishu.Entity.Student;

@Repository
public class StudentDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String  Save_query="insert into Student (Name,Fees) values(?,?)";
	private static final String  Update_query="update Student set Name=?,Fees =? where id =?";
	private static final String  Delete_query="delete from Student  where id =?";
	private static final String  Fetch_query="select * from Student";
	
	
	
	public void SaveStudent(Student student) {
		jdbcTemplate.update(Save_query, student.getName(),student.getFees());
	}
	
	public void updateStudent(Student student) {
		jdbcTemplate.update(Update_query, student.getName(),student.getFees());
	}
	public void DeleteStudent(int id) {
		jdbcTemplate.update(Delete_query,id);
	}
	private RowMapper<Student> rowMapper = new RowMapper<Student>() {

		public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
			Student e= new Student();
			e.setId(rs.getInt(1));
			e.setName(rs.getString(2));
			e.setFees(rs.getInt(3));
			return e;
		}
		
		
	};
	
	public List<Student> getAllStudent() {
			return jdbcTemplate.query(Fetch_query, rowMapper);
	}
		
}
