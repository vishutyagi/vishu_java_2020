package com.vishu.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vishu.Dao.StudentDao;
import com.vishu.Entity.Student;

@RestController
@RequestMapping("/students")
public class StudentController {
	
	@Autowired
	StudentDao studentDao;
	
	@PostMapping(value="/",consumes="application/json",
			produces="text/plain")
	public String SaveStudent(@RequestBody Student stu) {
		System.out.println(stu.getFees());
		System.out.println(stu.getName());
		studentDao.SaveStudent(stu);
		return "Student is save";
	}
	@PutMapping(value ="/",consumes ="application/json",produces ="text/plain")
	public String UpdateStudent(@RequestBody Student stu)throws Exception {
		studentDao.updateStudent(stu);
		return "Successfully Saved";
	}
	
	@GetMapping(value ="/{id}",produces ="text/plain")
	public String DeleteStudent(@PathVariable int id )throws Exception {
		studentDao.DeleteStudent(id);
		return "Successfully Delete";
	}
	
	@GetMapping(value="/" ,produces ="application/json")
	public List<Student> getAllStudent() {
		return studentDao.getAllStudent();
	}
	
		
	
	
	
}
