package com.vishu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiWithJdbcTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiWithJdbcTemplateApplication.class, args);
	}

}
