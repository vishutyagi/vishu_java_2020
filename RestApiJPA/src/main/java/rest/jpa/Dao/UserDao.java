package rest.jpa.Dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rest.jpa.Entity.User;

public interface UserDao extends CrudRepository<User, Integer> {
	
	public Iterable<User> findByPassword(String password);
	public Optional<User> findTopByOrderByIdDesc();
	public Optional<User>findByUserIdAndPassword(String userid ,String password);

}
