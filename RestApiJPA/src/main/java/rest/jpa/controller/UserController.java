package rest.jpa.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import rest.jpa.Dao.UserDao;
import rest.jpa.Entity.User;

@RestController
@RequestMapping(value ="Users")
public class UserController {
	
	@Autowired
	UserDao userdao;
	
	@PostMapping(value ="/")
	public String saveUser(@RequestBody User user) {
		userdao.save(user);
		return "sucessfulluy";
	}
	
	@GetMapping(value ="/")
	public Iterable<User> getUser(){
		return userdao.findAll();
	}
	
	@PutMapping(value="/")
	public String updateuser(@RequestBody User user) {
		userdao.save(user);
		return "update";
	} 
	
	@DeleteMapping(value ="/{id}")
	public String Deleteuser(@PathVariable int id) {
		userdao.deleteById(id);
		return "Delete successfully";
	}
	
	@GetMapping(value="/{id}")
	public User getuserbyid(@PathVariable int id) {
		Optional<User>opt = userdao.findById(id);
		if(opt.isPresent()) {
			return opt.get();
		}else {
			return null;
		}
	}
	
	@GetMapping(value ="/count")
	public long getAllcount() {
		return userdao.count();
	}
	
	@PostMapping(value ="/getAllJson")
	public String getAlltypejson(@RequestBody JsonNode jsonNode) {
		return jsonNode.toString();
	}
	
	@GetMapping(value="/findbyPassword")
	public Iterable<User> getAlluserbyPassword() {
		 return userdao.findByPassword("test");
		
	}
	
	@GetMapping(value="/findlastid")
	public int getlastid() {
		 Optional<User> user =userdao.findTopByOrderByIdDesc();
		 if(user.isPresent()) {
			 return user.get().getId();
		 }else {
			 return 0;
		 }
		
	}
	
	@GetMapping(value="/getByUseridAndPassword")
	public boolean getByUseridAndPassword() {
		 Optional<User> user =userdao.findByUserIdAndPassword("user002", "test2352");
		 if(user.isPresent()) {
			 return true;
		 }else {
			 return false;
		 }
		
	}

	
	
}
