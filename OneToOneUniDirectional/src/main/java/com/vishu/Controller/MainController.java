package com.vishu.Controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vishu.Dao.CountryDao;
import com.vishu.Entity.Country;

@RestController
@RequestMapping(value ="/countries")
public class MainController {
	
	@Autowired
	CountryDao countryDao;
	
	@PostMapping(value="/")
	public String saveCountryAndHeadOfState(@RequestBody Country con) {
		countryDao.save(con);
		return "successfully saved";
	}
	
	@GetMapping(value="/")
	public Iterable<Country>getAllCountryAndHos(){
		Iterable<Country> itr =countryDao.findAll();
		return itr;
	}
	
	@GetMapping(value="/{id}")
	public Country getCountryByID(@PathVariable int id) {
		Optional<Country> country = countryDao.findById(id);
		if(country.isPresent()) {
			return country.get();
		}else {
			return null;
		}
	}
	
	@PutMapping(value ="/")
	public String updateCountryAndHos(@RequestBody Country con) {
		countryDao.save(con);
		return "successfully update";
	}
	
	
}
