package com.vishu.Dao;

import org.springframework.data.repository.CrudRepository;

import com.vishu.Entity.Country;

public interface CountryDao extends CrudRepository<Country, Integer> {

}
