package com.vishu.Dao;

import org.springframework.data.repository.CrudRepository;

import com.vishu.Entity.HeadOfState;

public interface HeadOFStateDao extends CrudRepository<HeadOfState, Integer> {

}
