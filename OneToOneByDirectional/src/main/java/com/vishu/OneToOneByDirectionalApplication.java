package com.vishu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneToOneByDirectionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneToOneByDirectionalApplication.class, args);
	}

}
