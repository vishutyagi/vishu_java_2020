package com.vishu.Controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.vishu.Dao.CountryDao;
import com.vishu.Dto.CountryDto;
import com.vishu.Entity.Country;

@RestController
@RequestMapping(value ="/countries")
public class CountryController {
	
	@Autowired
	CountryDao countryDao;
	
	@PostMapping(value="/")
	public String saveCountryAndHeadOfState(@RequestBody Country con) {
		countryDao.save(con);
		return "successfully saved";
	}
	
	@GetMapping(value="/")
	public List<CountryDto>getAllCountryAndHos(){
		List<Country>cdto =Lists.newArrayList(countryDao.findAll());
		return cdto.stream()
				.map(Country -> toDto(Country))
				.collect(Collectors.toList());
	}
	public CountryDto toDto(Country country)
	{
		ModelMapper mapper = new ModelMapper();
		return mapper.map(country, CountryDto.class);
	}
	

//	  @GetMapping(value = "/") public Iterable<Country> getAllCountryAndHos() {
//	  return countryDao.findAll(); }
//	 
//	 
	
	@GetMapping(value="/{id}")
	public Country getCountryByID(@PathVariable int id) {
		Optional<Country> country = countryDao.findById(id);
		if(country.isPresent()) {
			return country.get();
		}else {
			return null;
		}
	}
	
	@PutMapping(value ="/")
	public String updateCountryAndHos(@RequestBody Country con) {
		countryDao.save(con);
		return "successfully update";
	}
	
	
}
