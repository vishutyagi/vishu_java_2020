package com.vishu.Controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.internal.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vishu.Dao.HeadOFStateDao;
import com.vishu.Dto.HeadOfStateDto;
import com.vishu.Entity.HeadOfState;

@RestController
@RequestMapping("/headOfState")
public class HeadOfStateController {
	
	@Autowired
	HeadOFStateDao hosd;
	
	@GetMapping(value= "/")
		public List<HeadOfStateDto> getallHos(){
		try {
			List<HeadOfState> list =com.google.common.collect.Lists.newArrayList(hosd.findAll());
			System.out.println(list);
			return list.stream()
					.map(HeadOfState->toDto(HeadOfState))
					.collect(Collectors.toList());
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	
	
	}
	public HeadOfStateDto toDto(HeadOfState headOfState)
	{
		ModelMapper mapper = new ModelMapper();
		return mapper.map(headOfState, HeadOfStateDto.class);
	}
	
	@GetMapping(value= "/biitrable")
	public Iterable<HeadOfState> getallheadofStae()	{
		return hosd.findAll();
	}
}
