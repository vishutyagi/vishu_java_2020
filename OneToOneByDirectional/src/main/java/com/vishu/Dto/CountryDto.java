package com.vishu.Dto;

import com.vishu.Entity.HeadOfState;

public class CountryDto {

	private int id;
	private String name;

//	private HeadOfStateDto hos; 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public HeadOfStateDto getHos() {
//		return hos;
//	}
//
//	public void setHos(HeadOfStateDto hos) {
//		this.hos = hos;
//	}

}


